# Medplum

## Trabalho Final de Gerência de Configuração - 2022.2

Este Projeto corresponde ao Trabalho Final da Disciplina Gerência de Configuração, com o objetivo de implementar a sua Integração Contínua (CI).

Como pré-requisitos para rodar esse projeto na sua máquina, faz-se necessário que você tenha instalado:

Git: https://git-scm.com
Node.js e npm: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
Docker: https://docs.docker.com/engine/install/

Uma vez instalados, você deve clonar o repositório remoto para a sua máquina com o comando:

```sh
git clone git@gitlab.com:ufc7/medplum.git
```

Em seguida, acesse a raiz do repositório na sua máquina e instale as dependências necessárias com o comando:

```sh
npm ci
```

Ainda na raiz, faça a build dos pacotes com o Turborepo com o comando:

```sh
npm run build
```

Assegure-se de que o Docker esteja funcionando e rode os serviços necessários para o backend a partir do docker-compose.yml na pasta raiz com o comando:

```sh
docker-compose up -d
```

Inicie os servidores com o comando:

```sh
cd packages/server
npm run dev
```

Para ter certeza de que tudo está funcionando, você pode acessar: http://localhost:8103/healthcheck

Se tudo estiver funcionando, você verá a seguinte mensagem no seu browser:

```sh
{ "ok": true, "postgres": true, "redis": true }
```

Por último, inicie a aplicação Web rodando os seguintes comandos a partir do diretório raiz:

```sh
cd packages/app
npm run dev
```

A aplicação poderá ser acessada em: http://localhost:3000/
O usuário padrão é "admin@example.com", e a senha padrão é "medplum_admin"

(:
